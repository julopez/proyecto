var expect = require('chai').expect;
var sinon = require('sinon');

var moduleLoader = require('./common/moduleLoader.js');
var mockFactory = require('./common/mockFactory.js');

var js = '../../../apiproxy/resources/jsc/JS-HttpClient.js';

describe('feature: Mock HttpClient', function () {

    it('should create Request object', sinon.test(function (done) {

        var mock = mockFactory.getMock();

        mock.httpClientSendMethod.withArgs(sinon.match({ url: 'https://httpbin.org/1?query=something' })).yields("request 1", null);
        mock.httpClientSendMethod.withArgs(sinon.match({ url: 'https://httpbin.org/2' })).yields("request 2, well written", null);

        moduleLoader.load(js, function (err) {
            expect(err).to.be.undefined;

            expect(mock.contextSetVariableMethod.calledWith('response.content', "request 2, well written")).to.be.true;
            expect(mock.contextSetVariableMethod.calledWith('response.content', "request 1")).to.be.true;
            done();
        });
    }));

});
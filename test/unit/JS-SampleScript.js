var expect = require('chai').expect;
var sinon = require('sinon');

var moduleLoader = require('./common/moduleLoader.js');
var mockFactory = require('./common/mockFactory.js');

var js = '../../../apiproxy/resources/jsc/JS-SampleScript.js';


describe('feature: Add data to payload', function() {

    it('should add username to payload', function(done) {
        var mock = mockFactory.getMock();

        mock.contextGetVariableMethod.withArgs('response.content').returns("{}");

        moduleLoader.load(js, function(err) {
            expect(err).to.be.undefined;

            expect(mock.contextSetVariableMethod.calledWith('response.content',JSON.stringify({username:"${user.name}"}, null, 4))).to.be.true;
            done();
        });
    });

});
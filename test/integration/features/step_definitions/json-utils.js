'use strict';

const apickli = require('apickli');

const jsonPath = require('JSONPath');
const path = require('path');
const fs = require('fs');

module.exports = function () {

    this.Then(/^response body as json should have path (.*) equals to (.*)$/, function (path, valueToCompare, callback) {
        // Write code here that turns the phrase above into concrete actions
        path = this.apickli.replaceVariables(path);
        valueToCompare = this.apickli.replaceVariables(valueToCompare);

        const body = JSON.parse(this.apickli.getResponseObject().body);
        const json = jsonPath({ json: body, path: path, resultType: 'value', autostart: true })[0];

        if (JSON.stringify(json) === JSON.stringify(JSON.parse(valueToCompare))) {
            callback();
        } else {
            callback('Objects not equal, received: ' + valueToCompare);
        }
    });

    this.Given(/^I pipe contents of file (.*) to variable (.*)$/, function (file, variable, callback) {

        file = this.apickli.replaceVariables(file);
        variable = this.apickli.replaceVariables(variable);

        fs.readFile(file, 'utf8', (err, data) => {
            if (err) {
                callback(err);
            } else {
                this.apickli.storeValueInScenarioScope(variable, data);
                callback();
            }
        });
    });
}

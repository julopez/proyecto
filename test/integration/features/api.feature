@api
Feature: API proxy test

  @dev
  @qa
  Scenario Outline: Confirm username property is in response payload
    Given I set X-Secure-Header header to some-valid-token
    When I GET /
    Then response code should be <response_code>
      And response body path $.username should be <username>
    Examples:
        | username    | response_code |
        | `username`  | 200           |
        | hramirez    | 200           |

  @dev
  Scenario Outline: Test anything endpoint
    Given I set X-Custom-Header header to <x-custom-header>
    When I POST to /
    Then response body path $.headers.X-Custom-Header should be <x-custom-header>
    Examples:
        | x-custom-header  |
        | asdfg            |
        | hramirez         |

  @dev
  Scenario Outline: Test greeting
    Given I set Content-Type header to <appjson>
      And I pipe contents of file <fixture> to body
      And I pipe contents of file <fixture> to variable payload
    When I POST to /
    Then response code should be 200
      And response body should be valid json
      And response body as json should have path $.json equals to `payload`
        Examples:
        | appjson           | fixture                                              |
        | application/json  | ./test/integration/features/fixtures/helloWorld.json |

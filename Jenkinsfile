pipeline {
    agent any    
    environment {
        TOKEN_APIGEE = credentials('apigee_token')
        TOKEN_SONAR = credentials('sonar_token')
        MAVEN_ACCESS = credentials('apigee_credentials')
        APIGEE_ORG = sh(returnStdout: true, script: '''
            echo -E $(cat pom.xml | grep "<apigee.org>" | sed 's/<[^>]*>//g' )
            ''').trim()
        PROXY_NAME = sh(returnStdout: true, script: '''
            echo -E $(cat pom.xml | grep "<name>" | sed 's/<[^>]*>//g' | cut -d '$' -f 1 )
            ''').trim()
        URL_SONAR = 'http://35.197.9.13:9000/'
        HOST = sh(returnStdout: true, script: '''
            HOST=$(echo $JENKINS_URL | awk -F[/:] '{print $4}')           
            echo "${HOST}"
            ''').trim() 
        BUILD_NAME =  sh(script:'''
            export TZ=America/Mexico_City;
            BUILD_NAME=$(date +%Y%m%d)
            if [ ! -f date_$BUILD_NAME ]; then
                touch date_$BUILD_NAME ;
                echo "1" > date_$BUILD_NAME ;
                if [ $(ls | grep date_  -i date_$BUILD_NAME -c ) >= 1 ]; then
                    rm --help;
                    rm $(ls | grep date_  -i date_$BUILD_NAME );
                fi;
            else
                echo $[$(cat date_$BUILD_NAME ) + 1] > date_$BUILD_NAME;
            fi;
            echo "$(echo $BUILD_NAME)_$(cat date_$BUILD_NAME)"
        ''',returnStdout:true).trim()
        AUTHOR = sh(script: 'git log -1 --pretty=%cn ${GIT_COMMIT} | sed "s/\\ /_/g"', returnStdout: true).trim()
        BRANCH_NAME = sh(returnStdout: true, script: 'echo $GIT_BRANCH | cut -d "/" -f 2').trim()
        LAST_COMMIT = sh(returnStdout: true, script: 'git log -n 1').trim()
        PROFILE = sh(returnStdout: true, script: '''
            BRANCH_NAME=$(echo $GIT_BRANCH | cut -d "/" -f 2)
            case $BRANCH_NAME in
                feature | developer | dev )
                    PROFILE="dev"
                ;;
                qa)
                    PROFILE="qa"
                ;;
                master)
                    PROFILE="prd"
                ;;"
            esac;
            echo "${PROFILE}"
            ''').trim()
        FROM=sh(returnStdout: true, script: '''
            PROFILE=$(echo $GIT_BRANCH | cut -d "/" -f 2)
            case $PROFILE in
                master)
                    FROM="preprod"
                ;;
                *)
                    FROM=""
                ;;
            esac;
            echo $FROM
            ''').trim()
        PATHSUFFIX = sh(returnStdout: true, script: '''
            BRANCH_NAME=$(echo $GIT_BRANCH | cut -d "/" -f 2)
            case $BRANCH_NAME in
                feature )
                    PATHSUFFIX="-$( echo $(git log -1 --pretty=%cn ${GIT_COMMIT}) | sed \"s/\\ /_/g\")"
                ;;
                *)
                    PATHSUFFIX=""
                ;;
            esac
            echo "${PATHSUFFIX}"
            ''').trim()
        APIGEE_OPTIONS = sh(returnStdout: true, script: '''
            BRANCH_NAME=$(echo $GIT_BRANCH | cut -d "/" -f 2)
            case $BRANCH_NAME in
                feature | developer | dev )
                    APIGEE_OPTIONS="override"
                ;;
                *)
                    APIGEE_OPTIONS="override"
                ;;
            esac;
            echo "${APIGEE_OPTIONS}"
            ''').trim()        
        UNIT_TEST = sh(returnStdout: true, script: '''
            if [ -d test/unit ]; then
                UNIT_TEST="TRUE"
            else
                UNIT_TEST="FALSE"
            fi;
            echo $UNIT_TEST;
        ''').trim()
        APICKLI = sh(returnStdout: true, script: '''
            if [ -d test/integration/features ]; then
                APICKLI="TRUE"
            else
                APICKLI="FALSE"
            fi
            echo $APICKLI;
        ''').trim()
        POSTMAN = sh(returnStdout: true, script: '''
            if [ -d tests ]; then
                POSTMAN="TRUE"
            else
                POSTMAN="FALSE"
            fi
            echo $POSTMAN;
        ''').trim()
        TAURUS = sh(returnStdout: true, script: '''
            if [ -d test/performance ]; then
                TAURUS="TRUE"
            else
                TAURUS="FALSE"
            fi
            echo $TAURUS;
        ''').trim()
        SPECS = sh(returnStdout: true, script: '''
            if [ -d specs ]; then
                SPECS="TRUE"
            else
                SPECS="FALSE"
            fi
            echo $SPECS;
        ''').trim()
    }    
    stages {
        stage('Process Resources') {
            agent {
                docker {
                    reuseNode true
                    image 'maven:3.6.3-openjdk-8'
                }
            }
            when {
                environment name:'APIGEE_ORG', value:'pefisa'
            }
            steps {
                script {
                    currentBuild.displayName = "${BUILD_NAME}"
                }                  
                sh 'if [ ! -f  my.properties ] ;  then touch  my.properties ;  fi;'
                sh 'curl --location --request GET "https://api.enterprise.apigee.com/v1/organizations/$APIGEE_ORG/apis/$PROXY_NAME/deployments" --header "Authorization: Basic $TOKEN_APIGEE" > ATUAL_VERSION_DEV.json'
                sh "echo $AUTHOR "
                sh "echo $PROXY_NAME"
                sh "echo $JOB_NAME"               
                sh "echo $WORKSPACE"
                sh(returnStdout: false, script: '''mvn -P $PROFILE process-resources  -Dapigee.username=$MAVEN_ACCESS_USR -Dapigee.password=$MAVEN_ACCESS_PSW''')                
                sh "echo $HOST"
            }
        }
        stage('Run Node Tooling') {
            parallel {                                           
                stage('Apigeelint') {    
                    agent { 
                        docker {
                            reuseNode true
                            image 'tworphom/nodejs_npm:2.0'
                            registryUrl 'https://index.docker.io/v1/'
                            registryCredentialsId 'dockerhub_credentials'
                            args '-u root:root'
                        }
                    }                     
                    steps {
                        sh 'npm run-script apigeelint'
                    }
                }
                stage('Jshint') {                            
                    agent { 
                        docker {
                            reuseNode true
                            image 'tworphom/nodejs_npm:2.0'
                            registryUrl 'https://index.docker.io/v1/'
                            registryCredentialsId 'dockerhub_credentials'
                            args '-u root:root'
                        }
                    }   
                    steps {
                        sh 'npm run-script jshint'
                    }
                }
                stage('JavaScript Unit Test') {                            
                    agent { 
                        docker {
                            reuseNode true
                            image 'tworphom/nodejs_npm:2.0'
                            registryUrl 'https://index.docker.io/v1/'
                            registryCredentialsId 'dockerhub_credentials'
                            args '-u root:root'
                        }
                    }  
                    when {
                        environment name:'UNIT_TEST', value:'TRUE'
                    }
                    steps {
                        sh 'npm run-script mocha'
                    }
                }
                stage('YAML Validator') {                            
                    agent { 
                        docker {
                            reuseNode true
                            image 'tworphom/nodejs_npm:2.0'
                            registryUrl 'https://index.docker.io/v1/'
                            registryCredentialsId 'dockerhub_credentials'
                            args '-u root:root'
                        }
                    }
                    when {
                        environment name:'SPECS', value:'TRUE'
                    }   
                    steps {
                        sh 'npm run-script yaml-test'
                    }
                }
            }                
        }    
        stage ('Sonar Analysis') {
            environment {
                scannerHome = tool 'SONAR_SCANNER'                
            }
            steps {
                withSonarQubeEnv('SONAR_GCP') {
                    sh '${scannerHome}/bin/sonar-scanner -e -Dsonar.projectKey=${PROXY_NAME} -Dsonar.host.url=${URL_SONAR} -Dsonar.login=$TOKEN_SONAR -Dsonar.sources=. -Dsonar.tests=. -Dsonar.test.inclusions=**/*test*/** -Dsonar.exclusions=**/*test*/**'                            
                }
                sleep(5)
                timeout(time: 1, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage ('Deploy Configs') {
            agent {
                docker {
                    reuseNode true
                    image 'maven:3.6.3-openjdk-8'
                }
            }
            steps {
                sh(returnStdout: false, script: '''
                mvn -P dev clean apigee-config:apiproducts apigee-config:developers apigee-config:apps apigee-config:exportAppKeys -Dapigee.config.option=create -Dapigee.username=$MAVEN_ACCESS_USR -Dapigee.password=$MAVEN_ACCESS_PSW 
                
                mvn -P $PROFILE clean process-resources apigee-config:kvms apigee-config:targetservers apigee-config:caches apigee-config:flowhooks apigee-enterprise:configure  -Dapigee.options=update -Dproxy.pathsuffix=$PATHSUFFIX -Dproxy.namesuffix=$PATHSUFFIX -Dapigee.username=$MAVEN_ACCESS_USR -Dapigee.password=$MAVEN_ACCESS_PSW 
                
                mvn -P $PROFILE clean process-resources apigee-config:maskconfigs -Dapigee.config.options=update -Dproxy.pathsuffix=$PATHSUFFIX -Dproxy.namesuffix=$PATHSUFFIX -Dapigee.username=$MAVEN_ACCESS_USR -Dapigee.password=$MAVEN_ACCESS_PSW
            ''')                                                
            }
        }     
        stage('Deploy Production') {
            environment{
                REVISION = sh(returnStdout: true, script: '''
                    REVISION=$(curl --location --request GET "https://api.enterprise.apigee.com/v1/organizations/$APIGEE_ORG/apis/$PROXY_NAME/deployments" --header "Authorization: Basic $TOKEN_APIGEE" | jq '.environment[0] | select(.name == "\'$FROM\'") | .revision[0].name ' | sed 's/\"//g');
                ''').trim()
            }
            parallel {               
                stage('API') {
                    when {
                        expression {
                            BRANCH_NAME ==~ /(master|release)/
                        }
                        anyOf {
                            environment name:'PROFILE', value: 'hml'
                            environment name:'PROFILE', value: 'prd'
                        }
                    }
                    steps {
                        sh "echo $REVISION";
                        sh 'curl --location --request POST "https://api.enterprise.apigee.com/v1/organizations/$APIGEE_ORG/environments/$PROFILE/apis/$PROXY_NAME/revisions/$REVISION/deployments?override=true&delay=0" \
                            --header "Authorization: Basic $TOKEN_APIGEE"'
                    }
                }
                stage('Maven') {
                    agent {
                        docker {
                            reuseNode true
                            image 'maven:3.6.3-openjdk-8'
                        }
                    }
                    when {
                        environment name:'PROFILE', value:'dev'
                    }
                    steps {
                        sh(returnStdout: false, script: '''
                            mvn -P $PROFILE process-resources -Dapigee.options=$APIGEE_OPTIONS -Dapigee.config.options=$APIGEE_OPTIONS -Dproxy.pathsuffix=$PATHSUFFIX -Dproxy.namesuffix=$PATHSUFFIX -Dapigee.username=$MAVEN_ACCESS_USR -Dapigee.password=$MAVEN_ACCESS_PSW 
                            
                            mvn -P $PROFILE clean process-resources apigee-enterprise:configure apigee-enterprise:deploy -Dapigee.options=$APIGEE_OPTIONS -Dproxy.pathsuffix=$PATHSUFFIX -Dproxy.namesuffix=$PATHSUFFIX -Dapigee.username=$MAVEN_ACCESS_USR -Dapigee.password=$MAVEN_ACCESS_PSW > LAST_DEPLOY.txt;
                            cat LAST_DEPLOY.txt;
                            LAST_DEPLOY=$(cat LAST_DEPLOY.txt | grep  'Deployed revision is' | cut -d ':' -f 2) ;
                            echo $LAST_DEPLOY > LAST_DEPLOY.txt;
                        ''')
                    }
                }
            }
        }
        stage('Integration Testing') {
            environment {
                ENV = sh(returnStdout:true, script:'''
                    ENV=""
                    if [ -f target/tests/*.postman_environment.json ];
                        then
                            ENV="--environment target/tests/*.postman_environment.json";
                    fi;
                    echo '$ENV';
                    ''')
                DATAS = sh(returnStdout:true, script:'''
                    DATAS=""
                    if [ -f target/tests/*.postman_scenario.json ];
                        then
                            DATAS="-d target/tests/*.postman_scenario.json";
                    else
                        if [ -f target/tests/*.postman_scenario.csv ];
                            then
                                DATAS="-d target/tests/*.postman_scenario.csv"
                        fi;
                    fi;
                    echo $DATAS;
                    ''')                
                REVISION_ATUAL = sh(returnStdout:true, script:'''
                    REVISION_ATUAL=$(cat ATUAL_VERSION_DEV.json | jq \'.environment[] | select(.name == "\'$PROFILE\'") | .revision[0].name \' | sed \'s/\"//g\') ;
                    rm ATUAL_VERSION_DEV.json;
                    echo $REVISION_ATUAL;
                    ''').trim()
                REVISION_NEW=sh(returnStdout:true, script:'''
                    if [ $PROFILE =  'dev' ] ;
                        then
                            LAST_DEPLOY=$(cat LAST_DEPLOY.txt);
                    else                        
                        LAST_DEPLOY=$(curl --location --request GET "https://api.enterprise.apigee.com/v1/organizations/$APIGEE_ORG/apis/$PROXY_NAME/deployments" --header "Authorization: Basic $TOKEN_APIGEE" | jq '.environment[] | select(.name == "\'$FROM\'") | .revision[0].name ' | sed 's/\"//g');
                    fi;
                    echo $LAST_DEPLOY''').trim()
            }
            parallel {
                stage('Apickli') {
                    agent { 
                        docker {
                            reuseNode true
                            image 'tworphom/nodejs_npm:2.0'
                            registryUrl 'https://index.docker.io/v1/'
                            registryCredentialsId 'dockerhub_credentials'
                            args '-u root:root'
                        }
                    }  
                    when {
                        environment name:'APICKLI', value:'TRUE'
                    }
                    steps {
                        sh 'npm run-script apickli'
                    }
                }
                stage('Newman / Postman') {
                    agent { 
                        docker {
                            reuseNode true
                            image 'tworphom/nodejs_npm:2.0'
                            registryUrl 'https://index.docker.io/v1/'
                            registryCredentialsId 'dockerhub_credentials'
                            args '-u root:root'
                        }
                    }  
                    when {
                        environment name:'POSTMAN', value:'TRUE'
                    }
                    steps {
                        sh 'npm run newman'
                    }
                }
            }
            post {
                failure {
                    sh(script:'''
                    if [ $REVISION_NEW != 1  ];
                        then
                            curl --location --request POST "https://api.enterprise.apigee.com/v1/organizations/$APIGEE_ORG/environments/$PROFILE/apis/$PROXY_NAME/revisions/$REVISION_ATUAL/deployments?override=true&delay=0" \
                            --header "Authorization: Basic $TOKEN_APIGEE";
                    fi;
                    curl --location --request DELETE "https://api.enterprise.apigee.com/v1/organizations/$APIGEE_ORG/environments/$PROFILE/apis/$PROXY_NAME/revisions/$REVISION_NEW/deployments" --header "Authorization: Basic $TOKEN_APIGEE";
                    if [ $PROFILE = 'dev'  ];
                        then
                            curl --location --request DELETE "https://api.enterprise.apigee.com/v1/organizations/$APIGEE_ORG/apis/$PROXY_NAME/revisions/$REVISION_NEW" --header "Authorization: Basic $TOKEN_APIGEE";
                    fi
                        ''')
                }
            }
        }
        stage('Performance Tests') {
            when {
                environment name:'TAURUS', value:'TRUE'
            }
            steps {                        
                sh "docker run --rm -v ${WORKSPACE}/tests/performance:/bzt-configs blazemeter/taurus performance_test.yml"
            }
        }
        stage('OWASP ZAP Security') {
            agent {
                docker {
                    reuseNode true
                    image 'owasp/zap2docker-stable'
                    args '-u root:sudo -v ${WORKSPACE}:/zap/wrk:rw'                           
                }
            }
            environment {
                        ENDPOINT=sh(returnStdout: true, script: '''
                            echo "$PROXY_NAME$PATHSUFFIX"

                            ''').trim()
            }
            steps {
                sh "zap-full-scan.py -t https://${APIGEE_ORG}-${PROFILE}.apigee.net/${ENDPOINT} -I -J -l PASS"
            }
        }
    }   
    post {
        always {            
            script {
                def mailRecipients = 'xxxxx@xxxxx.com; xxxxx@xxxxx.com'
                emailext body: '''${SCRIPT, template="template.template"}''',
                mimeType: 'text/html',
                subject: "[Jenkins] Pipeline ${JOB_NAME} - ${env.BUILD_DISPLAY_NAME} : ${currentBuild.result}!",
                to: "${mailRecipients}",
                from: 'apigee@xxxxx.com',
                replyTo: "${mailRecipients}",
                recipientProviders: [[$class: 'CulpritsRecipientProvider']]
            }
        }
        failure { 
            script { 
                if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'release') {
                emailext body: "${currentBuild.currentResult}: \nTarefa: ${env.JOB_NAME} \nBuild ${env.BUILD_DISPLAY_NAME}\nID: ${env.BUILD_ID}\nBranch: ${env.BRANCH_NAME}\n${env.LAST_COMMIT}\nMais informações em: ${env.RUN_DISPLAY_URL}",
                    to:'xxxxx@xxxxx.com',
                    from:'xxxxx@xxxxx.com',
                    subject: "APIGEE_JENKINS - [Jenkins] Pipeline ${JOB_NAME} - ${env.BUILD_DISPLAY_NAME} : ${currentBuild.result}!"        
                }
            }
        }
    }
}

var request1 = new Request('https://httpbin.org/1?query=something');
var request2 = new Request('https://httpbin.org/2');

var onComplete = function(response) {
    print('Printing response:');
    print(response);
    context.setVariable('response.content', response);
};

httpClient.send(request1, onComplete);
httpClient.send(request2, onComplete);

var payload = JSON.parse(context.getVariable('response.content'));

payload.username = '${user.name}';

context.setVariable('response.content', JSON.stringify(payload, null, 4));